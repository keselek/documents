<?php
/**
 * Created by PhpStorm.
 * User: keselek
 * Date: 29.03.2016
 * Time: 10:28
 */

$properties = array();

$tmp = array(
    'classKey'     => array(
        'type'  => 'textfield',
        'value' => '',
    ),
    'classKeyId'   => array(
        'type'  => 'numberfield',
        'value' => '',
    ),
    'Showparent'   => array(
        'type'  => 'combo-boolean',
        'value' => 'false',
    ),
    'innerDocList' => array(
        'type'  => 'textfield',
        'value' => 'tpl.Document.innerDocList',
    ),
    'DocList'      => array(
        'type'  => 'textfield',
        'value' => "tpl.Document.DocList",
    ),
);

foreach ($tmp as $k => $v) {
    $properties[] = array_merge(
        array(
            'name'    => $k,
            'desc'    => PKG_NAME_LOWER . '_prop_' . $k,
            'lexicon' => PKG_NAME_LOWER . ':properties',
        ), $v
    );
}

return $properties;