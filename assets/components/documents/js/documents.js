


var documents = {

    initialize: function(afConfig) {

        if(!jQuery().jGrowl) {
            document.write('<script src="'+afConfig.assetsUrl+'js/lib/jquery.jgrowl.min.js"><\/script>');
        }

        $(document).ready(function() {
            $.jGrowl.defaults.closerTemplate = '<div>[ '+afConfig.closeMessage+' ]</div>';
        });


    }

};
documents.Message = {
    success: function(message, sticky) {
        if (message) {
            if (!sticky) {sticky = false;}
            $.jGrowl(message, {theme: 'Doc-message-success', sticky: sticky});
        }
    }
    ,error: function(message, sticky) {
        if (message) {
            if (!sticky) {sticky = false;}
            $.jGrowl(message, {theme: 'Doc-message-error', sticky: sticky});
        }
    }
    ,info: function(message, sticky) {
        if (message) {
            if (!sticky) {sticky = false;}
            $.jGrowl(message, {theme: 'Doc-message-info', sticky: sticky});
        }
    }
    ,close: function() {
        $.jGrowl('close');
    }
};
actionurl = '/assets/components/documents/action.php';

function fotoramainit() {
    var $fotoramaDiv = $('#fotorama').fotorama();
    var fotorama = $fotoramaDiv.data('fotorama');
    $('.fotorama').on('fotorama:fullscreenenter', function (e, fotorama, extra) {
        $(".fotorama__fullscreen-icon").after("<div class='fotorama_custom__remove'><i style='font-size: 24px' class='fa fa-trash-o'></i></div>");
    });
    $('.fotorama').on('fotorama:fullscreenexit', function (e, fotorama, extra) {
        $("div.fotorama_custom__remove").remove()
    });
    return fotorama;
}


function uploaddocumentfiles(data, files) {
    var filedata = new FormData();
    var error = '';
    jQuery.each(files, function (i, file) {
        if (i > data.max_files - 1) {
            error = error + 'Превышено количество файлов';
        }
        if (file.name.length < 1) {
            //chAdmint.showError('Файл имеет неправильный размер','Ошибка');
            error = error + ' Файл имеет неправильный размер! ';
        } //Проверка на длину имени
        if (file.size > 5000000) {
            //chAdmint.showError('Слишком большой файл','Ошибка');
            error = error + ' File ' + file.name + ' Слишком большой';
        } //Проверка размера файла
        //if (file.type != 'image/png' && file.type != 'image/jpg' && !file.type != 'image/gif' && file.type != 'image/jpeg') {
        //    chAdmint.showError('PNG, JPG, GIF','Неправильный формат файла');
        //    error = error + 'File  ' + file.name + '  doesnt match png, jpg or gif';
        //} //Проверка типа файлов
        filedata.append(i, file);
    });
    filedata.append('id', data.data.id);
    filedata.append('type_id', data.data.type_id);
    if (error != '') {
        documents.Message.error(error);
    } else {
        $.ajax({
            url: actionurl + '?doc_action=Doc/UploadFiles',
            data: filedata,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            beforeSend: function () {
                //$('#preloader').show();
            },
            success: function (data) {
                data = $.parseJSON(data);
                if (data.success == 'true') {
                    documents.Message.success(data.message);
                }
                else {
                    documents.Message.error(data.message);
                }
            }
        });
    }
}

function addDocForm(id,parent){
    if (!fotorama.options) {
        fotorama = fotoramainit();
    }
   $('.SubDocuments').empty();
    if (id) {
        $.ajax({
            url: actionurl + '?doc_action=Doc/NewDoc',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                data = $.parseJSON(data);
                fotorama.destroy();
                $('.EditDocForm').append(data.data.formfields);
                $('#DocumentsFileUpload').attr('filemax', data.data.filemax);
                if(parent){
                    $('#submitDocForm').attr('parent',parent);
                }

            }
        });
    }
}

// Добавление нового документа///
$(document).on('click', '#addDocForm', function () {
    $('#submitDocForm').attr('parent','');
    $('#docform').attr('doc_id', '');
    $(".EditDocForm").empty();
    var id = $('#typedoc').val();
$('#docform').attr('type-id',id);
    addDocForm(id);

});

$(document).on('click', '#addDocSubForm', function () {
    $('#docform').attr('doc_id', '');
    var id = $('#subtype').val();
   $(".EditDocForm").empty();
    $('#docform').attr('type-id',id);
    var parent = $(this).attr('parent');
    addDocForm(id,parent);

});




// Сохранение документа///


$(document).on('click', '#submitDocForm', function () {


    data = $("#docform").serializeArray();

    var typeid =  $('#docform').attr('type-id');
    var doc_id = $('#docform').attr('doc_id');
    var files = $('#DocumentsFileUpload')[0].files;
    var parent = $('#submitDocForm').attr('parent');

    $.ajax({
        url: actionurl + '?doc_action=Doc/Save',
        type: 'POST',
        data: {data: data, typeid: typeid, doc_id: doc_id,parent:parent},
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success == 'true') {
                if (files.length > 0) {
                    data.max_files = $('#DocumentsFileUpload').attr('filemax');
                    uploaddocumentfiles(data, files);
                }
                documents.Message.success(data.message);
            }
            else {
                documents.Message.error(data.message);
            }
        }
    });
    return false;
});
//Просмотр документа///
$(document).on('click', '.openDoc', function () {

    if (!fotorama.options) {
        fotorama = fotoramainit();
    }

    $('#docform').attr('type-id','');
    var id = $(this).attr('id');
    // $(".EditDocForm").empty();
    $.ajax({
        url: actionurl + '?doc_action=Doc/OpenDoc',
        type: 'POST',
        data: {id: id},
        success: function (data) {

            $('.DocNoImageFile').empty();
            $('.SubDocuments').empty();
            data = $.parseJSON(data);
            if (data.success == 'true') {

                $('.EditDocForm').html(data.data.result);
                $("#docform :input").each(function () {
                    $(this).prop("disabled", true);
                });
                $('.fa-unlock').attr('class','fa fa-lock');
                $('#docform').attr('doc_id', id);
                $('#DocumentsFileUpload').attr('filemax', data.data.filemax);
                $('.DocNoImageFile').html(data.data.docfile);


                $('.SubDocuments').html(data.data.subtype);
                $('#addDocSubForm').attr('parent', data.data.doc_id);
                $('.SubDocuments').append(data.data.subdocs);
                $('#submitDocForm').attr('parent','');
                fotorama.destroy();
                $.each(data.data.pathfile, function (index, value) {
                    fotorama.push({img: data.data.thumbfile[index], full: value, thumb: data.data.thumbfile[index]});
                });

            }
        }
    });


});
//------- Разблокирование полей формы ---------
$(document).on('click', '.unlockDoc', function () {

    if ($(this).children().hasClass('fa-lock')) {
        $("#docform :input").each(function () {
            $(this).prop("disabled", false);
        });
        $(this).children().removeClass('fa-lock');
        $(this).children().addClass('fa-unlock');
    }
    else {
        $("#docform :input").each(function () {
            $(this).prop("disabled", true);
        });
        $(this).children().removeClass('fa-unlock');
        $(this).children().addClass('fa-lock');
    }
});
////Удаление документа -----
$(document).on('click', '.deleteDoc', function () {
    var id = $(this).attr('id');
    var thisdoc =$(this);

    $(".EditDocForm").empty();
    $.ajax({
        url: actionurl + '?doc_action=Doc/DeleteDoc',
        type: 'POST',
        data: {id: id},
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success == 'true') {
                documents.Message.success(data.message);
                thisdoc.parent().parent().remove();
            }
            else {
                documents.Message.error(data.message);
            }
        }
    });
});




function removefile(img,file){
    var doc_id=  $('#docform').attr('doc_id');
    $.ajax({
        url: actionurl + '?doc_action=Doc/DeleteDocFile',
        type: 'POST',
        data: {img: img,file:file,doc_id:doc_id},
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success == 'true') {
                documents.Message.success(data.message);
            }
            else {

            }
        }
    });
}

$(document).on('click', '.fotorama_custom__remove', function () {
    var img =fotorama.activeFrame.full;
    var activei = fotorama.activeIndex;
    fotorama.splice(activei, 1);
    removefile(img);

});

$(document).on('click', '.deleteDocFile', function () {
    var file = $(this).attr('path');
    removefile('',file);

});


