/**
 * Created by keselek on 23.03.2016.
 */
Documents.combo.Types = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        name: 'parent'
        , hiddenName: 'parent'
        , displayField: 'type'
        , valueField: 'id'
        , fields: ['id', 'type']
        , pageSize: 40,

        url: Documents.config.connector_url
        , baseParams: {
            action: 'mgr/documentstypes/getlist'
        },
        forceSelection: false,
    });
    Documents.combo.Types.superclass.constructor.call(this, config);

};
Ext.extend(Documents.combo.Types, MODx.combo.ComboBox);
Ext.reg('documents-combo-types', Documents.combo.Types);

Documents.combo.TypesField = function (config) {
    config = config || {};
    Ext.applyIf(config, {


        typeAhead: true,
        selectOnTab: true,
        store: [
            ['text', 'text'],
            ['date', 'date'],
            ['number', 'number']
        ],
        lazyRender: true,

        forceSelection: false
    });
    Documents.combo.TypesField.superclass.constructor.call(this, config);

};
Ext.extend(Documents.combo.TypesField, MODx.combo.ComboBox);
Ext.reg('documents-combo-types-field', Documents.combo.TypesField);

Documents.combo.Fieldslinks = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        name: 'field_id'
        , hiddenName: 'field_id'
        , displayField: 'field'
        , valueField: 'id'
        , fields: ['id', 'field']
        , pageSize: 40,

        url: Documents.config.connector_url
        , baseParams: {
            action: 'mgr/documentsfields/getlist'
        },
        forceSelection: false,
    });
    Documents.combo.Fieldslinks.superclass.constructor.call(this, config);

};
Ext.extend(Documents.combo.Fieldslinks, MODx.combo.ComboBox);
Ext.reg('documents-combo-fields-links', Documents.combo.Fieldslinks);


Documents.combo.TypesLinks = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        name: 'doc_type_id'
        , hiddenName: 'doc_type_id'
        , displayField: 'type'
        , valueField: 'id'
        , fields: ['id', 'type']
        , pageSize: 40,

        url: Documents.config.connector_url
        , baseParams: {
            action: 'mgr/documentstypes/getlist'
        },
        forceSelection: false,
    });
    Documents.combo.TypesLinks.superclass.constructor.call(this, config);

};
Ext.extend(Documents.combo.TypesLinks, MODx.combo.ComboBox);
Ext.reg('documents-combo-types-links', Documents.combo.TypesLinks);


Documents.combo.Jsonrender = function (config) {
    console.log(config);
    config = config || {};
    Ext.applyIf(config, {
        type: 'string',

        typeAhead: true,
        selectOnTab: true,
        renderer: this.renderJson,
        lazyRender: true,

        forceSelection: true
    });
    Documents.combo.TypesField.superclass.constructor.call(this, config);

};
Ext.extend(Documents.combo.Jsonrender, MODx.combo.ComboBox, {
    renderJson: function (value, props, row) {
        console.log(value);
        if (value != '') {
            return JSON.stringify(value);
        }
        else {
            return '';
        }


    }
});


Ext.reg('documents-combo-json', Documents.combo.Jsonrender);