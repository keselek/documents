Documents.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',

		stateful: true,
		 stateId: 'documents-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},

		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('documents') + '</h2>',
			cls: '',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
			items: [{
				// Имя вкладки
				title: _('document_types')
				// Массив с содержимым
				, items: [
					// Блок HTML с описанием этой вкладки
					{
						html: _('document_types_intro')
						, border: false
						, bodyCssClass: 'panel-desc'
						, bodyStyle: 'margin-bottom: 10px'
					}
					// Вызов рабочей таблицы
					, {
						xtype: 'document-grid-types'
						, preventRender: true
					}]
			}, {
				// Имя вкладки
				title: _('document_type_fields')
				// Массив с содержимым
				, items: [
					// Блок HTML с описанием этой вкладки
					{
						html: _('document_type_fields_intro')
						, border: false
						, bodyCssClass: 'panel-desc'
						, bodyStyle: 'margin-bottom: 10px'
					}
					// Вызов рабочей таблицы
					, {
						xtype: 'document-grid-fields'
						, preventRender: true
					}]
			}, {
				// Имя вкладки
				title: _('documents__type_Links')
				// Массив с содержимым
				, items: [
					// Блок HTML с описанием этой вкладки
					{
						html: _('documents__type_Links_intro')
						, border: false
						, bodyCssClass: 'panel-desc'
						, bodyStyle: 'margin-bottom: 10px'
					}
					// Вызов рабочей таблицы
					, {
						xtype: 'document-grid-links'
						, preventRender: true
					}]
			}]
		}]
	});
	Documents.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(Documents.panel.Home, MODx.Panel);
Ext.reg('documents-panel-home', Documents.panel.Home);
