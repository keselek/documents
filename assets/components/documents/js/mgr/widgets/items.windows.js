Documents.window.CreateItem = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'documents-documentsType-window-create';
	}
	Ext.applyIf(config, {
		title: _('documents-documentsType-window-create'),
		width: 550,
		autoHeight: true,
		url: Documents.config.connector_url,
		action: 'mgr/documentstypes/create',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	Documents.window.CreateItem.superclass.constructor.call(this, config);
};
Ext.extend(Documents.window.CreateItem, MODx.Window, {

	getFields: function (config) {
		return [{xtype: 'hidden', name: 'id', id: config.id + '-id',},
			{
				xtype: 'textfield',
				fieldLabel: _('documents_documentsType_name'),
				name: 'type',
				id: config.id + '-type',
				anchor: '99%',
				allowBlank: false
			},
			{
				layout: 'column'
				, border: false
				, anchor: '100%'
				, items: [{
				columnWidth: .5
				, layout: 'form'
				, defaults: {msgTarget: 'under'}
				, border: false
				, items: [
					{
						xtype: 'textfield',
						fieldLabel: _('documents_documentsType_maxfiles'),
						name: 'max_files',
						id: config.id + '-max_files',
						anchor: '99%'
					}
				]
			}, {
				columnWidth: .5
				, layout: 'form'
				, defaults: {msgTarget: 'under'}
				, border: false
				, items: [
					{
						xtype: 'documents-combo-types',
						fieldLabel: _('documents_documentsType_parent'),
						name: 'parent',
						id: config.id + '-parent',
						anchor: '99%',
					}]
			}]
			},
			{
				xtype: 'textarea',
				fieldLabel: _('documents_documentsType_validators'),
				name: 'validators',
				id: config.id + '-validators',
				anchor: '99%',
				height: 50,
			}
		];
	},

	loadDropZones: function() {
	}

});
Ext.reg('documents-documentsType-window-create', Documents.window.CreateItem);


Documents.window.UpdateItem = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'documents-documentsType-window-update';
	}
	Ext.applyIf(config, {
		title: _('documents_documentsType_update'),
		width: 550,
		autoHeight: true,
		url: Documents.config.connector_url,
		action: 'mgr/documentstypes/update',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	Documents.window.UpdateItem.superclass.constructor.call(this, config);
};
Ext.extend(Documents.window.UpdateItem, MODx.Window, {

	getFields: function (config) {

		return [{xtype: 'hidden', name: 'id', id: config.id + '-id',},
			{
				xtype: 'textfield',
				fieldLabel: _('documents_documentsType_name'),
				name: 'type',
				id: config.id + '-type',
				anchor: '99%',
				allowBlank: false
			},
			{
				layout: 'column'
				, border: false
				, anchor: '100%'
				, items: [{
				columnWidth: .5
				, layout: 'form'
				, defaults: {msgTarget: 'under'}
				, border: false
				, items: [
					{
						xtype: 'textfield',
						fieldLabel: _('documents_documentsType_maxfiles'),
						name: 'max_files',
						id: config.id + '-max_files',
						anchor: '99%'
					}
				]
			}, {
				columnWidth: .5
				, layout: 'form'
				, defaults: {msgTarget: 'under'}
				, border: false
				, items: [
					{
						xtype: 'documents-combo-types',
						fieldLabel: _('documents_documentsType_parent'),
						name: 'parent',
						id: config.id + '-parent',
						anchor: '99%',
					}]
			}]
			},
			{
				xtype: 'textarea',
				fieldLabel: _('documents_documentsType_validators'),
				name: 'validators',
				id: config.id + '-validators',
				anchor: '99%',
				height: 50,

				//renderer: Ext.util.JSON.encode(),
				//type: "string",
					//renderer: this.renderJson
			}
		];
	},
	renderJson: function (value, props, row) {
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/documentstypes/get',
				id: 25
			},
			listeners: {
				success: {
					fn: function (r) {
						var w = MODx.load({
							xtype: 'textfield',
							id: Ext.id(),
							record: r,
							listeners: {
								success: {
									fn: function () {
										this.refresh();
									}, scope: this
								}
							}
						});
					}, scope: this
				}
			}
		});
	},
	loadDropZones: function() {
	},


});
Ext.reg('documents-documentsType-window-update', Documents.window.UpdateItem);





Documents.window.Linkcreate = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'documents-docLink-window-create';
	}
	Ext.applyIf(config, {
		title: _('documents__type_Links_create'),
		width: 550,
		autoHeight: true,
		url: Documents.config.connector_url,
		action: 'mgr/documentslinks/create',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	Documents.window.Linkcreate.superclass.constructor.call(this, config);
};
Ext.extend(Documents.window.Linkcreate, MODx.Window, {

	getFields: function (config) {
		return [{xtype: 'hidden', name: 'id', id: config.id + '-id',},
			{
				layout: 'column'
				, border: false
				, anchor: '100%'
				, items: [{
				columnWidth: .5
				, layout: 'form'
				, defaults: {msgTarget: 'under'}
				, border: false
				, items: [
					{
						xtype: 'textfield',
						fieldLabel: _('documents_docLink_classKey'),
						name: 'classKey',
						id: config.id + '-classKey',
						anchor: '99%'
					}
				]
			}, {
				columnWidth: .5
				, layout: 'form'
				, defaults: {msgTarget: 'under'}
				, border: false
				, items: [
					{
						xtype: 'documents-combo-types-links',
						fieldLabel: _('documents_docLink_id'),
						name: 'doc_type_id',
						id: config.id + '-doc_type_id',
						anchor: '99%',
					}]
			}]
			}
		];
	},

	loadDropZones: function () {
	}

});
Ext.reg('documents-docLink-window-create', Documents.window.Linkcreate);
