/**
 * Created by keselek on 25.03.2016.
 */
Documents.window.CreateFieldLink = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'documents-documentsField-Link-window-create';
    }

    Ext.applyIf(config, {
        title: _('documents__type_field_window_create'),
        width: 550,
        autoHeight: true,
        url: Documents.config.connector_url,
        baseParams: {
            action: 'mgr/documentstypesfields/create',
            type_id: config.type_id,
        },

        fields: this.getFields(config),

    });
    Documents.window.CreateFieldLink.superclass.constructor.call(this, config);
};
Ext.extend(Documents.window.CreateFieldLink, MODx.Window, {

    getFields: function (config) {
        return [{xtype: 'hidden', name: 'id', id: config.id + '-id',},
            {
                layout: 'column'
                , border: false
                , anchor: '100%'
                , items: [{
                columnWidth: .5
                , layout: 'form'
                , defaults: {msgTarget: 'under'}
                , border: false
                , items: [
                    {
                        xtype: 'documents-combo-fields-links',
                        fieldLabel: _('documents__type_field'),
                        name: 'field',

                        anchor: '99%'
                    }
                ]
            }]
            }
        ];
    },

    loadDropZones: function () {
    }

});
Ext.reg('documents-documentsField-Link-window-create', Documents.window.CreateFieldLink);
Documents.grid.TypeFields = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'documents-documents-Type-Field-grid';
    }

    Ext.applyIf(config, {

        title: _('document_type_fields_Links'),
        width: 550,
        url: Documents.config.connector_url,
        fields: this.getFields(config),
        columns: this.getColumns(config),
        tbar: this.getTopBar(config),
        sm: new Ext.grid.CheckboxSelectionModel(),
        baseParams: {
            action: 'mgr/documentstypesfields/getlist',
            id: config.record
        },
        listeners: {
            //rowDblClick: function (grid, rowIndex, e) {
            //	var row = grid.store.getAt(rowIndex);
            //	this.updateItem(grid, e, row);
            //}

        },
        viewConfig: {
            forceFit: true,
            enableRowBody: true,
            autoFill: true,
            showPreview: true,
            scrollOffset: 0,
            //getRowClass: function (rec, ri, p) {
            //	return !rec.data.active
            //		? 'documents-grid-row-disabled'
            //		: '';
            //}
        },
        paging: true,
        remoteSort: true,
        autoHeight: true,
        save_action: 'mgr/documentstypesfields/update',
        autosave: true,

    });

    Documents.grid.Type.superclass.constructor.call(this, config);

    // Clear selection on grid refresh
    //this.store.on('load', function () {
    //    if (this._getSelectedIds().length) {
    //        this.getSelectionModel().clearSelections();
    //    }
    //}, this);
};


Ext.extend(Documents.grid.TypeFields, MODx.grid.Grid, {
    windows: {},
    getMenu: function (grid, rowIndex) {
        var m = [];
        m.push('-');
        m.push({
            text: _('documents__type_field_remove')
            , handler: this.removeItem
        });
        this.addContextMenuItem(m);
    },
    createItem: function (btn, e) {

        var w = MODx.load({
            xtype: 'documents-documentsField-Link-window-create',
            type_id: btn.type_id,
            id: Ext.id(),
            listeners: {
                success: {
                    fn: function () {

                        this.refresh();
                                // this.reset();


                    },scope: this
                }
            }
        });
        w.reset();
        w.setValues({active: true});
        w.show(e.target);
    },

    removeItem: function (act, btn, e) {
        var ids = this._getSelectedIds();
        if (!ids.length) {
            return false;
        }
        MODx.msg.confirm({
            title: ids.length > 1
                ? _('documents__type_field_remove')
                : _('documents__type_field_remove'),
            text: ids.length > 1
                ? _('documents__type_field_remove_confirm')
                : _('documents__type_field_remove_confirm'),
            url: this.config.url,
            params: {
                action: 'mgr/documentstypesfields/remove',
                ids: Ext.util.JSON.encode(ids),
            },
            listeners: {
                success: {
                    fn: function (r) {
                        this.refresh();
                    }, scope: this
                }
            }
        });
        return true;
    },

    _getSelectedIds: function () {
        var ids = [];
        var selected = this.getSelectionModel().getSelections();

        for (var i in selected) {
            if (!selected.hasOwnProperty(i)) {
                continue;
            }
            ids.push(selected[i]['id']);
        }

        return ids;
    },
    getFields: function (config) {
        return ['id', 'type_id', 'field_id'];
    },

    getColumns: function (config) {
        return [{
            header: _('documents_documentsType_id'),
            dataIndex: 'id',
            sortable: true,
            width: 30
        }, {
            header: _('documents__type_field'),
            dataIndex: 'field_id',
            sortable: false,
            width: 60,
            editor: {xtype: 'documents-combo-fields-links'}
        }];
    },
    getTopBar: function (config) {
        return [{
            text: '<i class="icon icon-plus"></i>&nbsp;' + _('documents__type_field_create_link'),
            handler: this.createItem,
            type_id: config.record,
            scope: this
        }];
    },

});

Ext.reg('documents-documents-Type-Field-grid', Documents.grid.TypeFields);

Documents.window.TypeFields = function (config) {
    config = config || {};
    if (!config.id) {
        config.id = 'documents-documents-Type-Field-window-grid';
    }
    Ext.applyIf(config, {
        title: _('documents_documentsType_update'),
        width: 600
        ,buttons: [{
        text: config.cancelBtnText || _('close')
        ,scope: this
        ,handler: function() { config.closeAction !== 'close' ? this.hide() : this.close(); }
    }],
        baseParams:{},
        autoHeight: true,
          fields: {xtype: 'documents-documents-Type-Field-grid', record: config.record},

    });
    Documents.window.TypeFields.superclass.constructor.call(this, config);
};

Ext.extend(Documents.window.TypeFields, MODx.Window, {
    
});

Ext.reg('documents-documents-Type-Field-window-grid', Documents.window.TypeFields);