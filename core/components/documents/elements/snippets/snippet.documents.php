<?php
/** @var array $scriptProperties */
/** @var Documents $Documents */
if (!$Documents = $modx->getService('documents', 'Documents', $modx->getOption('documents_core_path', null, $modx->getOption('core_path') . 'components/documents/') . 'model/documents/', $scriptProperties)) {
	return 'Could not load Documents class!';
}


$classKey = $modx->getOption('classKey', $scriptProperties, '');
$classKeyId = $modx->getOption('classKeyId', $scriptProperties, '');
$DocTypeListRow = $modx->getOption('DocTypeListRow', $scriptProperties, 'tpl.Document.DocTypeListRow');
$DocTypeList = $modx->getOption('DocTypeList', $scriptProperties, '
tpl.Document.DocTypeList');
$DocForm = $modx->getOption('DocForm', $scriptProperties, 'tpl.Document.DocForm');


$typeres='';
$_SESSION['documentsForm'] = array('classKey'=>$classKey,'classKeyId'=> $classKeyId);
$q = $modx->newQuery('DocumentType');
if($classKey){
	$q->innerJoin('DocumentsTypeLink', 'DocLink', '`DocumentType`.`id` = `DocLink`.`doc_type_id`');
	$q->where(array('`DocLink`.`classKey`' => $classKey));
}


$q->where(array('`DocumentType`.`parent`' => ""));
$q->prepare();

$types = $modx->getCollection('DocumentType',$q);
$result = array();

foreach($types as $type){
	$typeres .= $modx->getChunk($DocTypeListRow, array('value' => $type->get('id'), 'type' => $type->get('type')));
}

$types = $modx->getChunk($DocTypeList, array('types' => $typeres, 'id' => 'typedoc', 'buttonid' => 'addDocForm'));
$output = $modx->getChunk($DocForm, array('types' => $types));


$Documents->loadJsCss();
return $output;








