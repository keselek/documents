<?php
class DocumentsCreateProcessor extends modObjectCreateProcessor
{
    public $objectType = 'Document';
    public $classKey = 'Document';
    public $languageTopics = array('documents');
    public $afterSaveEvent = 'DocumentsOnSave';
    public $beforeSaveEvent = 'DocumentsOnBeforeSave';
    //public $permission = 'create';


    /**
     * @return bool
     */

    public function beforeSave()
    {

        $typeId = $this->object->get('type_id');
        $type = $this->modx->getObject('DocumentType', array('id' => $typeId));

        if (!$type->validate($this->object)) {
            return 'Не прошло проверку валидации';
        }
        return parent::beforeSave();

    }


}

return 'DocumentsCreateProcessor';