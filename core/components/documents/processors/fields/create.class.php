<?php
class DocumentsFieldCreateProcessor extends modObjectCreateProcessor
{
    public $objectType = 'DocumentField';
    public $classKey = 'DocumentField';
    public $languageTopics = array('documents');
    public $afterSaveEvent = 'DocumentFieldOnSave';
    public $beforeSaveEvent = 'DocumentFieldOnBeforeSave';
    //public $permission = 'create';


    /**
     * @return bool
     */


}

return 'DocumentsFieldCreateProcessor';