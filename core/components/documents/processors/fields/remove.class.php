<?php

/**
 * Remove an Items
 */
class DocumentsFieldRemoveProcessor extends modObjectProcessor {
    public $objectType = 'DocumentField';
    public $classKey = 'DocumentField';
    public $languageTopics = array('documents');
    public $afterSaveEvent = 'DocumentFieldOnRemove';
    public $beforeSaveEvent = 'DocumentFieldOnBeforeRemove';
    //public $permission = 'remove';


    /**
     * @return array|string
     */
    public function process() {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }

            /** @var DocumentsItem $object */

        if (!$objects = $this->modx->getCollection($this->classKey, array('doc_id' => $this->getProperty('doc_id')))) {
                return $this->failure($this->modx->lexicon('documents_item_err_nf'));
            }
        foreach ($objects as $object) {

            $object->remove();
        }



        return $this->success();
    }

}

return 'DocumentsFieldRemoveProcessor';