<?php
require_once MODX_CORE_PATH . 'model/modx/processors/security/user/getlist.class.php';
class DocumentsLinkGetListProcessor extends modObjectGetListProcessor {
    public $objectType = 'Document';
    public $classKey = 'Document';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';
    //public $permission = 'list';


    /**
     * * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return boolean|string
     */
    public function beforeQuery() {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }
    public function prepareQueryBeforeCount(xPDOQuery $c){
        if( $this->getProperty('classKey') and $this->getProperty('classKey_id') ){
            $c->where(array('`Document`.`classKey`' => $this->getProperty('classKey')));
            $c->where(array('`Document`.`classKey_id`' => $this->getProperty('classKey_id')));


        }

        if( $this->getProperty('parent')){
            $c->where(array('`Document`.`parent`' => $this->getProperty('parent')));
        }
        else{
            if( $this->getProperty('showparent') ==1){

            }
            elseif($this->getProperty('showparent') ==0){
                $c->where(array('`Document`.`parent`' => 0));
            }
        }


        $c->prepare();
        $c->stmt->execute();

        return $c;
    }

    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */



    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object) {
        $array = $object->toArray();

        return $array;
    }

}

return 'DocumentsLinkGetListProcessor';