<?php

/**
 * Created by PhpStorm.
 * User: keselek
 * Date: 24.03.2016
 * Time: 16:50
 */
class DocumentsTypeFieldGetProcessor extends modObjectGetProcessor
{
    public $objectType = 'DocumentTypeField';
    public $classKey = 'DocumentTypeField';
    public $languageTopics = array('documents:default');
    //public $permission = 'view';


    /**
     * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return mixed
     */
    public function process()
    {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }

        return parent::process();
    }

}

return 'DocumentsTypeFieldGetProcessor';