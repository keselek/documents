<?php

/**
 * Create an Item
 */
class DocumentsItemCreateProcessor extends modObjectCreateProcessor
{
    public $objectType = 'DocumentType';
    public $classKey = 'DocumentType';
    public $languageTopics = array('documents');
    //public $permission = 'create';


    /**
     * @return bool
     */
    public function beforeSet()
    {
        $name = trim($this->getProperty('type'));
        if (empty($name)) {
            $this->modx->error->addField('type', $this->modx->lexicon('documents_item_err_name'));
        } elseif ($this->modx->getCount($this->classKey, array('type' => $name))) {
            $this->modx->error->addField('type', $this->modx->lexicon('documents_item_err_ae'));
        }

        return parent::beforeSet();
    }

}

return 'DocumentsItemCreateProcessor';