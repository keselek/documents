<?php

/**
 * Created by PhpStorm.
 * User: keselek
 * Date: 25.03.2016
 * Time: 16:21
 */
class DocumentsTypeFieldsGetProcessor extends modObjectUpdateProcessor
{
    public $objectType = 'DocumentTypeFieldLink';
    public $classKey = 'DocumentTypeFieldLink';
    public $languageTopics = array('documents');
    public $primaryKeyField = 'id';

    //public $permission = 'save';

    public function initialize()
    {

        $data = $this->getProperties();

        if (is_array($data['data'])) {

        } else {
            $data = $this->modx->fromJSON($data['data']);
            foreach ($data as $key => $prop) {
                $this->setProperty($key, $prop);
            }
        }


        return parent::initialize();
    }

    /**
     * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return bool|string
     */
    public function beforeSave()
    {

        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @return bool
     */

}

return 'DocumentsTypeFieldsGetProcessor';
