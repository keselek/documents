<?php

/**
 * Remove an Items
 */
class DocumentsRemoveProcessor extends modObjectProcessor {
    public $objectType = 'Document';
    public $classKey = 'Document';
    public $languageTopics = array('documents');
    //public $permission = 'remove';


    /**
     * @return array|string
     */
    public function process() {

        /** @var DocumentsItem $object */
        if (!$object = $this->modx->getObject($this->classKey, $this->getProperty('id'))) {
            return $this->failure($this->modx->lexicon('documents_item_err_nf'));
        }

        $object->remove();

        return $this->success();
    }

}

return 'DocumentsRemoveProcessor';