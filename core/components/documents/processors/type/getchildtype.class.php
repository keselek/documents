<?php
/**
 * Created by PhpStorm.
 * User: Denisov Nikita
 * Date: 16.03.2016
 * Time: 16:54
 */
require_once MODX_CORE_PATH . 'model/modx/processors/security/user/getlist.class.php';
class DocumentsGetChildProcessor extends modObjectGetListProcessor {
    public $objectType = 'DocumentType';
    public $classKey = 'DocumentType';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';
    //public $permission = 'list';


    /**
     * * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return boolean|string
     */
    public function beforeQuery() {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */



    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object) {
        $array = $object->toArray();

        return $array;
    }

}

return 'DocumentsGetChildProcessor';