<?php
require_once MODX_CORE_PATH . 'model/modx/processors/security/user/getlist.class.php';
class DocumentsGetListProcessor extends modObjectGetListProcessor {
    public $objectType = 'DocumentType';
    public $classKey = 'DocumentType';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';
    public $option = 'parent';
    //public $permission = 'list';


    /**
     * * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return boolean|string
     */
    public function beforeQuery() {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */



    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object) {
        $array = $object->toArray();

        return $array;
    }
    public function prepareQueryBeforeCount(xPDOQuery $c) {
        $parent = $this->getProperty($this->option,false);
        $c->innerJoin('DocumentsTypeLink', 'DocLink', '`DocumentType`.`id` = `DocLink`.`doc_type_id`');
        if ($this->getProperty('classKey')) {
            $c->where(array('`Document`.`classKey`' => $this->getProperty('classKey')));
            $c->where(array('`Document`.`classKey_id`' => $this->getProperty('classKey_id')));
        }
        if ($parent) {
            $c->where(array(
                'parent' => $parent,

            ));
        }
        $c->prepare();
        $c->stmt->execute();

        return $c;
    }


}

return 'DocumentsGetListProcessor';