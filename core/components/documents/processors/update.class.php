<?php

class DocumentsUpdateProcessor extends modObjectUpdateProcessor {
    public $objectType = 'Document';
    public $classKey = 'Document';
    public $languageTopics = array('documents');
    public $afterSaveEvent = 'DocumentsOnUpdate';
    public $beforeSaveEvent = 'DocumentsOnBeforeUpdate';
    //public $permission = 'save';


    /**
     * We doing special check of permission
     * because of our objects is not an instances of modAccessibleObject
     *
     * @return bool|string
     */
    public function beforeSave() {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return true;
    }


    /**
     * @return bool
     */

}

return 'DocumentsUpdateProcessor';
